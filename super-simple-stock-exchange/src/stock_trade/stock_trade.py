from datetime import datetime, time
from enum import Enum

class Indicator(Enum):
    BUY = 0
    SELL = 1


class StockTrade:
    def __init__(self, symbol: str, quantity: int, indicator: Indicator, price: float, timestamp = datetime.now()) -> None:
        self.symbol = symbol
        self.quantity = quantity
        self.indicator = indicator
        self.price = price
        self.timestamp = timestamp

    # All Setters and Getters

    def __repr__(self) -> str:
        return "StockTrade(Symbol: " + self.symbol + ", Quantity: " + str(self.quantity) + ", Indicator: " + str(self.indicator) + ", Price: " + str(self.price) + ", Timestamp: " + str(self.timestamp) + ")"
