from datetime import datetime, timedelta
from stock_trade.stock_trade import StockTrade
from stock_exchange.stock_exchange import StockExchange
from stock_exchange.stock import Stock, Type

class StockService:
    def __init__(self) -> None:
        self.exchange = StockExchange()
        self.fill_sample_stocks()

    def fill_sample_stocks(self):
        self.exchange.stocks["TEA"] = Stock("TEA", Type.COMMON, 0, 0, 100)
        self.exchange.stocks["POP"] = Stock("POP", Type.COMMON, 8, 0, 100)
        self.exchange.stocks["ALE"] = Stock("ALE", Type.COMMON, 23, 0, 60)
        self.exchange.stocks["GIN"] = Stock("GIN", Type.PREFERRED, 8, 2, 100)
        self.exchange.stocks["JOE"] = Stock("JOE", Type.COMMON, 13, 0, 250)

    def calc_dividend_yield(self, symbol: str, price: int):
        stock: Stock = self.exchange.stocks.get(symbol)
        if stock.type is Type.COMMON:
            return stock.last_dividend / price
        elif stock.type is Type.PREFERRED:
            return stock.fixed_dividend * stock.par_value / price
        
        return 0

    def calc_pe_ratio(self, symbol: str, price: int):
        stock: Stock = self.exchange.stocks.get(symbol)
        pe_ratio = 0
        try:
            pe_ratio = price / stock.last_dividend
        except ZeroDivisionError:
            pe_ratio = None
        
        return pe_ratio

    def record_trade(self, stock_trade: StockTrade):
        if stock_trade.symbol not in self.exchange.trades:
            self.exchange.trades[stock_trade.symbol] = []

        self.exchange.trades[stock_trade.symbol].append(stock_trade)
        return True

    def calc_volume_weighted_stock_price(self, symbol: str, last_minutes: int):
        quantities = 0
        weighted_price = 0
        current_time = datetime.now()
        for stock_trade in self.exchange.trades[symbol]:
            if current_time - stock_trade.timestamp <= timedelta(minutes=last_minutes):
                quantities += stock_trade.quantity
                weighted_price += (stock_trade.quantity * stock_trade.price)
        
        return weighted_price / quantities

