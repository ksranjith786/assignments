# Assignments

```bash
$ python src/main.py
Stocks {'TEA': Stock(Symbol: TEA, Type: Type.COMMON, Last Dividend: 0, Fixed Dividend: 0%, Par Value: 100), 'POP': Stock(Symbol: POP, Type: Type.COMMON, Last Dividend: 8, Fixed Dividend: 0%, Par Value: 100), 'ALE': Stock(Symbol: ALE, Type: Type.COMMON, Last Dividend: 23, Fixed Dividend: 0%, Par Value: 60), 'GIN': Stock(Symbol: GIN, Type: Type.PREFERRED, Last Dividend: 8, Fixed Dividend: 2%, Par Value: 100), 'JOE': Stock(Symbol: JOE, Type: Type.COMMON, Last Dividend: 13, Fixed Dividend: 0%, Par Value: 250)}
Trades {}
Trades {'POP': [StockTrade(Symbol: POP, Quantity: 3, Indicator: Indicator.BUY, Price: 154, Timestamp: 2021-10-12 11:50:51.622102), StockTrade(Symbol: POP, Quantity: 1, Indicator: Indicator.SELL, Price: 140, Timestamp: 2021-10-12 11:40:51.636179), StockTrade(Symbol: POP, Quantity: 6, Indicator: Indicator.BUY, Price: 125, Timestamp: 2021-10-12 10:30:51.636179)], 'GIN': [StockTrade(Symbol: GIN, Quantity: 11, Indicator: Indicator.BUY, Price: 238, Timestamp: 2021-10-12 11:50:51.622102)], 'ALE': [StockTrade(Symbol: ALE, Quantity: 1, Indicator: Indicator.SELL, Pricator: Indicator.SELL, Price: 300, Timestamp: 2021-10-12 11:43:05.320515), StockTrade(Symbol: ALE, Quantity: 1, Indicator: Indicator.BUY, Price: 762, Timestamp: 2021-10-12 11:50:05.286377)], 'TEA': [StockTrade(Symbol: TEA, Quantity: 7, Indicator: Indicator.SELL, Price: 1900, Timestamp: 2021-10-12 11:45:05.320515)]}
#
Div Yield of ALE 0.046
Div Yield of JOE 0.043333333333333335
PE Ratio of JOE 23.076923076923077
PE Ratio of TEA None
Volume Weighted Stock Price of POP 150.5
```
