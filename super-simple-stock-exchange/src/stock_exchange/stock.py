from enum import Enum

class Type(Enum):
    COMMON = 0
    PREFERRED = 1
    

class Stock:
    def __init__(self, symbol: str, type: Type, last_dividend: float, fixed_dividend: float, par_value: float) -> None:
        self.symbol = symbol
        self.type = type
        self.last_dividend = last_dividend
        self.fixed_dividend = fixed_dividend
        self.par_value = par_value

    # All Setters and Getters

    def __repr__(self) -> str:
        return "Stock(Symbol: " + self.symbol + ", Type: " + str(self.type) + ", Last Dividend: " + str(self.last_dividend) + ", Fixed Dividend: " + str(self.fixed_dividend) + "%, Par Value: " + str(self.par_value) + ")"

